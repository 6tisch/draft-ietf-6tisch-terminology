



6TiSCH                                               MR. Palattella, Ed.
Internet-Draft                                                      LIST
Intended status: Informational                                P. Thubert
Expires: September 3, 2018                                         cisco
                                                             T. Watteyne
                                       Linear Technology / Dust Networks
                                                                 Q. Wang
                                         Univ. of Sci. and Tech. Beijing
                                                           March 2, 2018


        Terminology in IPv6 over the TSCH mode of IEEE 802.15.4e
                    draft-ietf-6tisch-terminology-09

Abstract

   This document provides a glossary of terminology used in IPv6 over
   the TSCH mode of IEEE 802.15.4e (6TiSCH).  This document extends
   existing terminology documents for Low-power and Lossy Networks.

Status of This Memo

   This Internet-Draft is submitted in full conformance with the
   provisions of BCP 78 and BCP 79.

   Internet-Drafts are working documents of the Internet Engineering
   Task Force (IETF).  Note that other groups may also distribute
   working documents as Internet-Drafts.  The list of current Internet-
   Drafts is at https://datatracker.ietf.org/drafts/current/.

   Internet-Drafts are draft documents valid for a maximum of six months
   and may be updated, replaced, or obsoleted by other documents at any
   time.  It is inappropriate to use Internet-Drafts as reference
   material or to cite them other than as "work in progress."

   This Internet-Draft will expire on September 3, 2018.

Copyright Notice

   Copyright (c) 2018 IETF Trust and the persons identified as the
   document authors.  All rights reserved.

   This document is subject to BCP 78 and the IETF Trust's Legal
   Provisions Relating to IETF Documents
   (https://trustee.ietf.org/license-info) in effect on the date of
   publication of this document.  Please review these documents
   carefully, as they describe your rights and restrictions with respect
   to this document.  Code Components extracted from this document must



Palattella, et al.      Expires September 3, 2018               [Page 1]

Internet-Draft             6tisch-terminology                 March 2018


   include Simplified BSD License text as described in Section 4.e of
   the Trust Legal Provisions and are provided without warranty as
   described in the Simplified BSD License.

Table of Contents

   1.  Introduction  . . . . . . . . . . . . . . . . . . . . . . . .   2
   2.  Terminology . . . . . . . . . . . . . . . . . . . . . . . . .   2
   3.  Security Considerations . . . . . . . . . . . . . . . . . . .   8
   4.  References  . . . . . . . . . . . . . . . . . . . . . . . . .   8
     4.1.  Normative References  . . . . . . . . . . . . . . . . . .   8
     4.2.  Informative References  . . . . . . . . . . . . . . . . .   9
     4.3.  External Informative References . . . . . . . . . . . . .  10
   Authors' Addresses  . . . . . . . . . . . . . . . . . . . . . . .  10

1.  Introduction

   The IEEE802.15.4 Medium Access Control (MAC) has evolved with the
   Time Slotted Channel Hopping (TSCH) mode for industrial-type
   applications.  It provides deterministic capabilities to the point
   that a packet that pertains to a certain flow crosses the network
   from node to node following a very precise schedule, like a train
   leaves intermediate stations at precise times along its path.

   This document provides additional terminology elements to cover terms
   that are new to the context of TSCH wireless networks and other
   deterministic networks.

2.  Terminology

   The draft extends [RFC7102] and use terms from [RFC6550] and
   [RFC6552], which are all included here by reference.

   The draft does not reuse terms from IEEE802.15.4e such as "path" or
   "link" which bear a meaning that is quite different from classical
   IETF parlance.

   This document adds the following terms:

   6TiSCH:     IPv6 over the Timeslotted Channel Hopping (TSCH) mode of
               IEEE802.15.4e.  It defines (i) the 6top sublayer; (ii) a
               set of protocols for setting up a TSCH schedule in
               distributed approach, for managing the allocation of
               resources; and (iii) the architecture to bind them
               together, for use in IPv6 TSCH based networks.

   6top:       The "6TiSCH Operation Sublayer" (6top) is the next
               highest layer of the IEEE802.15.4e TSCH medium access



Palattella, et al.      Expires September 3, 2018               [Page 2]

Internet-Draft             6tisch-terminology                 March 2018


               control layer.  It implements and terminates the "6top
               Protocol" (6P), and contains a "6top Scheduling Function"
               (SF).

   SF:         The "6top Scheduling Function" (SF) "is the cell
               management entity that add or delete cells dynamically
               based on its allocation policy in order to fulfill cell
               requirements.  The cell negotiation with a neighbor is
               done using 6P.  General guidelines for designing a SF are
               provided in [I-D.ietf-6tisch-6top-protocol].

   SFID:       The "6top Scheduling Function Identifier" (SFID) is a
               4-bit field identifying a SF.  Defined in
               [I-D.ietf-6tisch-6top-protocol].

   6P:         The "6top Protocol" (6P) allows neighbor nodes to
               communicate to add/delete cells to one another in their
               TSCH schedule.  Defined in
               [I-D.ietf-6tisch-6top-protocol].

   6P Transaction:  Part of the "6top Protocol" (6P), the action of two
               neighbors exchanging a 6P request message and the
               corresponding 6P response message.  Defined in
               [I-D.ietf-6tisch-6top-protocol].

   ASN:        Absolute Slot Number, the total number of timeslots that
               have elapsed since the PAN coordinator has started the
               TSCH network.  Incremented by one at each timeslot.  It
               is wide enough to not roll over in practice.  See
               [IEEE802154-2015] and [RFC7554].

   Blacklist of Frequencies:  A set of frequencies which should not be
               used for communication.  See [IEEE802154-2015] and
               [RFC7554].

   BBR:        Backbone Router.  In the 6TiSCH architecture, an LBR and
               also a IPv6 ND-efficiency-aware Router (NEAR)
               [I-D.chakrabarti-nordmark-6man-efficient-nd].  Performs
               ND proxy operations between registered devices and
               classical ND devices that are located over the backbone.

   Broadcast Cell:  A scheduled cell used for broadcast transmission.

   Bundle:     A group of equivalent scheduled cells, i.e. cells
               identified by different [slotOffset, channelOffset],
               which are scheduled for a same purpose, with the same
               neighbor, with the same flags, and the same slotframe.
               The size of the bundle refers to the number of cells it



Palattella, et al.      Expires September 3, 2018               [Page 3]

Internet-Draft             6tisch-terminology                 March 2018


               contains.  For a given slotframe length, the size of the
               bundle translates directly into bandwidth.  A bundle is a
               local abstraction thar represents a half-duplex link for
               either sending or receiving, with bandwidth that amounts
               to the sum of the cells in the bundle.  A bundle is
               globally identified by (source MAC, destination MAC,
               TrackID).  At Layer 3, a pair of bundles forms a link.
               By using a well-known constant, NULLT, as TrackId for a
               L3 link, the IP link between adjacent nodes A and B
               comprises 2 bundles: (macA, macB, NULLT) and (macB, macA,
               NULLT).  At Layer 2, a pair of bundles forms a switching
               state.  Considered a segment A-B-C along a track, there
               are two bundles in node B, one incoming = (macA, macB,
               trackId) and one outgoing = (macB, macC, trackId).

   CCA:        Clear Channel Assessment.  Mechanism defined in
               [IEEE802154-2015], section 6.2.5.2.  In a TSCH network,
               CCA can be used to detect other radio networks in
               vicinity.  Nodes listen the channel before sending, to
               detect other ongoing transmissions.  Because the network
               is synchronized, CCA cannot be used to detect colliding
               transmission within the same network.  CCA is necessary
               for the 6TiSCH minimal configuration
               [I-D.ietf-6tisch-minimal] in shared slots, and in
               presence of multiple instances of 6TiSCH networks.

   Cell:       A single element in the TSCH schedule, identified by a
               slotOffset, a channelOffset, a slotframeHandle.  A cell
               can be scheduled or unscheduled.

   Centralized Cell Reservation:  A reservation of a cell done by a
               centralized entity (e.g., a PCE) in the network.

   Centralized Track Reservation:  A reservation of a track done by a
               centralized entity (e.g., a PCE) in the network.

   ChannelOffset:  Identifies a row in the TSCH schedule.  The number of
               available channelOffset values is equal to the number of
               available frequencies.  The channelOffset translates into
               a frequency when the communication takes place, resulting
               in channel hopping.  See [RFC7554].

   Channel Distribution/Usage (CDU) matrix:  : Matrix of cells (i,j)
               representing the spectrum (channel) distribution among
               the different nodes in the 6TiSCH network.  The CDU
               matrix has width in timeslots, equal to the period of the
               network scheduling operation, and height equal to the
               number of available channels.  Every cell (i,j) in the



Palattella, et al.      Expires September 3, 2018               [Page 4]

Internet-Draft             6tisch-terminology                 March 2018


               CDU, identified by (slotOffset, channelOffset), belongs
               to a specific chunk.  It has to be noticed that such a
               matrix which includes all the cells grouped in chunks,
               belonging to different slotframes, is different from the
               TSCH schedule.

   Chunk:      A well-known list of cells, distributed in time and
               frequency, within a CDU matrix; a chunk represents a
               portion of a CDU matrix.  The partition of the CDU in
               chunks is globally known by all the nodes in the network
               to support the appropriation process, which is a
               negotiation between nodes within an interference domain.
               A node that manages to appropriate a chunk gets to decide
               which transmissions will occur over the cells in the
               chunk within its interference domain (i.e., a parent node
               will decide when the cells within the appropriated chunk
               are used and by which node, among its children.

   Dedicated Cell:  A cell that is reserved for a given node to transmit
               to a specific neighbor.

   Deterministic Network:  The generic concept of deterministic network
               is defined in [I-D.ietf-detnet-architecture].  When
               applied to 6TiSCH it refers to the reservation of tracks
               which guarantee an end to end latency and optimize the
               PDR for well-characterized flows.

   Distributed Cell Reservation:  A reservation of a cell done by one or
               more in-network entities (typically a connection
               endpoint).

   Distributed Track Reservation:  A reservation of a track done by one
               or more in-network entities (typically a connection
               endpoint).

   EB:         Enhanced Beacon frame used by a node to announce the
               presence of the network.  It contains enough information
               for a joining node to synchronize to the network.  See
               [IEEE802154-2015] and [RFC7554].

   Hard Cell:  A scheduled cell which the 6top sublayer cannot relocate.

   Hopping Sequence:  Ordered sequence of frequencies, identified by a
               Hopping_Sequence_ID, used for channel hopping, when
               translating the channel offset value into a frequency
               (i.e., PHY channel).  See [IEEE802154-2015] and
               [RFC7554].




Palattella, et al.      Expires September 3, 2018               [Page 5]

Internet-Draft             6tisch-terminology                 March 2018


   IE:         Information Element, a Type-Length-Value containers
               placed at the end of the MAC header, used to pass data
               between layers or devices.  Some IE identifiers are
               managed by the IEEE [IEEE802154-2015].  Some IE
               identifiers are managed by the IETF
               [I-D.kivinen-802-15-ie].

   JP:         The Join Proxy (JP) is a one-hop neighbor of a joining
               node that may facilitate it to become meaningful part of
               the network (e.g., by serving as a local connectivity
               point to the remainder of the network).  JP emits EBs,
               used by Pledges to synchronize to the network.  See
               [I-D.ietf-6tisch-minimal-security] and
               [I-D.ietf-6tisch-dtsecurity-secure-join].

   JRC:        The Join Registrar/Coordinator (JRC) is a central entity
               that coordinates the joining of new nodes in the network.
               See [I-D.ietf-6tisch-minimal-security] and
               [I-D.ietf-6tisch-dtsecurity-secure-join].

   Join Protocol:  The protocol which secures initial communication
               between a joining node and the JRC.

   LBR:        Low-power Lossy Network (LLN) Border Router.  It is an
               LLN device, usually powered, that acts as a Border Router
               to the outside within the 6TiSCH architecture.

   Link:       A communication facility or medium over which nodes can
               communicate at the link layer, i.e., the layer
               immediately below IP.  Thus, the IETF parlance for the
               term "Link" is adopted, as opposed to the IEEE802.15.4e
               terminology.

   Operational Network:  A IEEE802.15.4e network whose encryption/
               authentication keys are determined by some algorithms/
               protocols.  There may be network-wide group keys, or per-
               link keys.

   Pledge:     The Pledge is a device attempting to join a particular
               6TiSCH network.  See [I-D.ietf-6tisch-minimal-security].

   (to) Relocate a Cell:  The action operated by the 6top sublayer of
               changing the slotOffset and/or channelOffset of a soft
               cell.

   (to) Schedule a Cell:  The action of turning an unscheduled cell into
               a scheduled cell.




Palattella, et al.      Expires September 3, 2018               [Page 6]

Internet-Draft             6tisch-terminology                 March 2018


   Scheduled cell:  A cell which is assigned a neighbor MAC address
               (broadcast address is also possible), and one or more of
               the following flags: TX, RX, shared, timeskeeping.  A
               scheduled cell can be used by the IEEE802.15.4e TSCH
               implementation to communicate.  A scheduled cell can be
               either a hard or a soft cell.

   Shared Cell:  A cell marked with both the "TX" and "shared" flags.
               This cell can be used by more than one transmitter node.
               A back-off algorithm is used to resolve contention.  See
               [IEEE802154-2015] and [RFC7554].

   SlotOffset: Identifies a column in the TSCH schedule, i.e., the
               number of timeslots since the beginning of the current
               iteration of the slotframe.  See [IEEE802154-2015] and
               [RFC7554].

   Slotframe:  A collection of timeslots repeating in time, analogous to
               a superframe in that it defines periods of communication
               opportunities.  It is characterized by a slotframe_ID,
               and a slotframe_size.  Multiple slotframes can coexist in
               a node's schedule, i.e., a node can have multiple
               activities scheduled in different slotframes, based on
               the priority of its packets/traffic flows.  The timeslots
               in the Slotframe are indexed by the SlotOffset; the first
               timeslot is at SlotOffset 0.  See [IEEE802154-2015] and
               [RFC7554].

   Soft Cell:  A scheduled cell which the 6top sublayer can relocate.

   Timeslot:   A basic communication unit in TSCH which allows a
               transmitter node to send a frame to a receiver neighbor,
               and that receiver neighbor to optionally send back an
               acknowledgment.  See [IEEE802154-2015] and [RFC7554].

   Time Source Neighbor:  A neighbor that a node uses as its time
               reference, and to which it needs to keep its clock
               synchronized.  See [IEEE802154-2015] and [RFC7554].

   Track:      A determined sequence of cells along a multi-hop path.
               It is typically the result of a track reservation.  The
               node that initializes the process of establishing a track
               is the owner of the track.  The latter assigns a unique
               identifier to the track, called TrackID.

   TrackID:    Unique identifier of a track, assigned by the owner of
               the track.




Palattella, et al.      Expires September 3, 2018               [Page 7]

Internet-Draft             6tisch-terminology                 March 2018


   TSCH:       Time Slotted Channel Hopping, a medium access mode of the
               [IEEE802154-2015] standard which uses time
               synchronization to achieve ultra low-power operation and
               channel hopping to enable high reliability.  See
               [IEEE802154-2015] and [RFC7554].

   TSCH Schedule:  A matrix of cells, each cell indexed by a slotOffset
               and a channelOffset.  The TSCH schedule contains all the
               scheduled cells from all slotframes and is sufficient to
               qualify the communication in the TSCH network.  The
               number of channelOffset values (the "height" of the
               matrix) is equal to the number of available frequencies.
               See [IEEE802154-2015] and [RFC7554].

   Unscheduled Cell:  A cell which is not used by the IEEE802.15.4e TSCH
               implementation.  See [IEEE802154-2015] and [RFC7554].

3.  Security Considerations

   Since this document specifies terminology and does not specify new
   procedures or protocols, it raises no new security issues.

4.  References

4.1.  Normative References

   [RFC2309]  Braden, B., Clark, D., Crowcroft, J., Davie, B., Deering,
              S., Estrin, D., Floyd, S., Jacobson, V., Minshall, G.,
              Partridge, C., Peterson, L., Ramakrishnan, K., Shenker,
              S., Wroclawski, J., and L. Zhang, "Recommendations on
              Queue Management and Congestion Avoidance in the
              Internet", RFC 2309, DOI 10.17487/RFC2309, April 1998,
              <https://www.rfc-editor.org/info/rfc2309>.

   [RFC3444]  Pras, A. and J. Schoenwaelder, "On the Difference between
              Information Models and Data Models", RFC 3444,
              DOI 10.17487/RFC3444, January 2003,
              <https://www.rfc-editor.org/info/rfc3444>.

   [RFC6550]  Winter, T., Ed., Thubert, P., Ed., Brandt, A., Hui, J.,
              Kelsey, R., Levis, P., Pister, K., Struik, R., Vasseur,
              JP., and R. Alexander, "RPL: IPv6 Routing Protocol for
              Low-Power and Lossy Networks", RFC 6550,
              DOI 10.17487/RFC6550, March 2012,
              <https://www.rfc-editor.org/info/rfc6550>.






Palattella, et al.      Expires September 3, 2018               [Page 8]

Internet-Draft             6tisch-terminology                 March 2018


   [RFC6552]  Thubert, P., Ed., "Objective Function Zero for the Routing
              Protocol for Low-Power and Lossy Networks (RPL)",
              RFC 6552, DOI 10.17487/RFC6552, March 2012,
              <https://www.rfc-editor.org/info/rfc6552>.

   [RFC6775]  Shelby, Z., Ed., Chakrabarti, S., Nordmark, E., and C.
              Bormann, "Neighbor Discovery Optimization for IPv6 over
              Low-Power Wireless Personal Area Networks (6LoWPANs)",
              RFC 6775, DOI 10.17487/RFC6775, November 2012,
              <https://www.rfc-editor.org/info/rfc6775>.

   [RFC7102]  Vasseur, JP., "Terms Used in Routing for Low-Power and
              Lossy Networks", RFC 7102, DOI 10.17487/RFC7102, January
              2014, <https://www.rfc-editor.org/info/rfc7102>.

   [RFC7554]  Watteyne, T., Ed., Palattella, M., and L. Grieco, "Using
              IEEE 802.15.4e Time-Slotted Channel Hopping (TSCH) in the
              Internet of Things (IoT): Problem Statement", RFC 7554,
              DOI 10.17487/RFC7554, May 2015,
              <https://www.rfc-editor.org/info/rfc7554>.

4.2.  Informative References

   [I-D.chakrabarti-nordmark-6man-efficient-nd]
              Chakrabarti, S., Nordmark, E., Thubert, P., and M.
              Wasserman, "IPv6 Neighbor Discovery Optimizations for
              Wired and Wireless Networks", draft-chakrabarti-nordmark-
              6man-efficient-nd-07 (work in progress), February 2015.

   [I-D.ietf-6tisch-6top-protocol]
              Wang, Q., Vilajosana, X., and T. Watteyne, "6top Protocol
              (6P)", draft-ietf-6tisch-6top-protocol-09 (work in
              progress), October 2017.

   [I-D.ietf-6tisch-dtsecurity-secure-join]
              Richardson, M., "6tisch Secure Join protocol", draft-ietf-
              6tisch-dtsecurity-secure-join-01 (work in progress),
              February 2017.

   [I-D.ietf-6tisch-minimal]
              Vilajosana, X., Pister, K., and T. Watteyne, "Minimal
              6TiSCH Configuration", draft-ietf-6tisch-minimal-21 (work
              in progress), February 2017.








Palattella, et al.      Expires September 3, 2018               [Page 9]

Internet-Draft             6tisch-terminology                 March 2018


   [I-D.ietf-6tisch-minimal-security]
              Vucinic, M., Simon, J., Pister, K., and M. Richardson,
              "Minimal Security Framework for 6TiSCH", draft-ietf-
              6tisch-minimal-security-04 (work in progress), October
              2017.

   [I-D.ietf-detnet-architecture]
              Finn, N., Thubert, P., Varga, B., and J. Farkas,
              "Deterministic Networking Architecture", draft-ietf-
              detnet-architecture-04 (work in progress), October 2017.

   [I-D.kivinen-802-15-ie]
              Kivinen, T. and P. Kinney, "IEEE 802.15.4 Information
              Element for IETF", draft-kivinen-802-15-ie-06 (work in
              progress), March 2017.

   [I-D.thubert-6lo-rfc6775-update-reqs]
              Thubert, P. and P. Stok, "Requirements for an update to
              6LoWPAN ND", draft-thubert-6lo-rfc6775-update-reqs-07
              (work in progress), April 2016.

   [I-D.thubert-roll-forwarding-frags]
              Thubert, P. and J. Hui, "LLN Fragment Forwarding and
              Recovery", draft-thubert-roll-forwarding-frags-02 (work in
              progress), September 2013.

4.3.  External Informative References

   [IEEE802154-2015]
              IEEE standard for Information Technology, "IEEE Std
              802.15.4-2015 Standard for Low-Rate Wireless Personal Area
              Networks (WPANs)", December 2015.

Authors' Addresses

   Maria Rita Palattella (editor)
   Luxembourg Institute of Science and Technology
   Department 'Environmental Research and Innovation' (ERIN)
   41, rue du Brill
   Belvaux  L-4422
   Luxembourg

   Phone: (+352) 275 888-5055
   Email: mariarita.palattella@list.lu







Palattella, et al.      Expires September 3, 2018              [Page 10]

Internet-Draft             6tisch-terminology                 March 2018


   Pascal Thubert
   Cisco Systems, Inc
   Village d'Entreprises Green Side
   400, Avenue de Roumanille
   Batiment T3
   Biot - Sophia Antipolis  06410
   France

   Phone: +33 497 23 26 34
   Email: pthubert@cisco.com


   Thomas Watteyne
   Linear Technology / Dust Networks
   30695 Huntwood Avenue
   Hayward, CA  94544
   USA

   Phone: +1 (510) 400-2978
   Email: twatteyne@linear.com


   Qin Wang
   Univ. of Sci. and Tech. Beijing
   30 Xueyuan Road
   Beijing  100083
   China

   Phone: +86 (10) 6233 4781
   Email: wangqin@ies.ustb.edu.cn





















Palattella, et al.      Expires September 3, 2018              [Page 11]
